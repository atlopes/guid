* install itself
IF !("\GUID.FXP") $ SET("Procedure")
	SET PROCEDURE TO (SYS(16)) ADDITIVE
ENDIF

DEFINE CLASS GUID AS Custom

	Value = .NULL.

	FUNCTION Init
	
		DECLARE Long CoCreateGuid IN Ole32.dll String @ guid
		DECLARE Long StringFromGUID2 IN Ole32.dll String guid, String @ unicodeGuid, Integer cchMax
		DECLARE Long CLSIDFromString IN Ole32.dll String unicodeGuid, String @ qGuid
	
	ENDFUNC

	FUNCTION Create
	LPARAMETERS TextGUID AS String

		LOCAL BinaryGUID
	
		m.BinaryGUID = REPLICATE(CHR(0), 16)

		IF PCOUNT() = 1

			IF CLSIDFromString(STRCONV(m.TextGUID,5), @m.BinaryGUID) != 0
				m.BinaryGUID = .NULL.
			ENDIF

		ELSE

			This.Value = REPLICATE(CHR(0), 16)
			CoCreateGuid(@m.BinaryGUID)

		ENDIF
		
		This.Value = m.BinaryGUID
				
		RETURN This.Value
	
	ENDFUNC
	
	FUNCTION ToString AS String
	
		LOCAL UnicodeGUID AS String
		LOCAL Length AS Integer

		IF !ISNULL(This.Value)	
			m.UnicodeGUID = REPLICATE(CHR(0), 80)
			m.Length = StringFromGUID2(This.Value, @m.UnicodeGUID, 40)
			IF m.Length > 0
				RETURN STRCONV(LEFT(m.UnicodeGUID, (m.Length - 1) * 2), 6)
			ENDIF
		ENDIF
		
		RETURN .NULL.
	
	ENDFUNC


ENDDEFINE

